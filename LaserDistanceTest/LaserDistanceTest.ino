// Sharp IR GP2Y0A41SK0F Distance Test
// http://tinkcore.com/sharp-ir-gp2y0a41-skf/

#include <SimpleKalmanFilter.h>

#define sensor A0 // Sharp IR  (10-80cm, analog)
#define RELAY1 2
#define RELAY2 3
#define poti1 A1
#define poti2 A2

SimpleKalmanFilter simpleKalmanFilter(2, 2, 0.01);
SimpleKalmanFilter filterPoti1(2, 2, 0.01);
SimpleKalmanFilter filterPoti2(2, 2, 0.01);

float filteredCloseAdjustment;
float filteredFarAdjustment;

const long SERIAL_REFRESH_TIME = 100;
long refresh_time;
bool farDetect, closeDetect;
void setup() {
    Serial.begin(115200);
    pinMode(RELAY1, OUTPUT);
    pinMode(RELAY2, OUTPUT);
    digitalWrite(RELAY1, HIGH);
    digitalWrite(RELAY2, HIGH);
    farDetect = false;
    closeDetect = false;
    
}

void loop() {
  
  // 5v
  float volts = analogRead(sensor)*0.0048828125;  // value from sensor * (5/1024)
  float distance = pow((0.0425*volts - 0.0075), -1); // worked out from datasheet graph

  float closeAdj = 11+round(0.0390 * analogRead(poti1));
  float farAdj =  15+round(0.0439 * analogRead(poti2));

  
   if(distance < 10){
    distance = 10;
  }
  if(distance > 70){
    distance = 70;
  }
  
  float real_value = distance;
  // float measured_value = real_value + random(-100,100)/100.0;

  
  float measured_value = distance;
  float estimated_value = simpleKalmanFilter.updateEstimate(measured_value);
  filteredCloseAdjustment = filterPoti1.updateEstimate(closeAdj);
  filteredFarAdjustment = filterPoti2.updateEstimate(farAdj);
  
 
  //delay(200); // slow down serial port 
  /*
  digitalWrite(RELAY1, HIGH);
  digitalWrite(RELAY2, LOW);
  delay(1000);
  digitalWrite(RELAY1, LOW);
  digitalWrite(RELAY2, HIGH);
  delay(1000);
  */
  if (millis() > refresh_time) {
    Serial.print("Volts: ");
    Serial.println(volts,1);
    Serial.println(estimated_value,1);   // print the distance
    Serial.print("closeAdj: ");
    Serial.println(filteredCloseAdjustment,1);
    Serial.print("farAdj: ");
    Serial.println(filteredFarAdjustment,1);
    if (estimated_value > filteredFarAdjustment - int(farDetect)*0.8){ // small histeresis (in cm)
      digitalWrite(RELAY1, HIGH);
      farDetect = true;
     
    }
    else
    {
      digitalWrite(RELAY1, LOW);
      farDetect = false;
    }

    if (estimated_value < filteredCloseAdjustment + int(closeDetect)*0){  // histeresis of 0.3 cm (in cm)
      closeDetect = true;
      digitalWrite(RELAY2, HIGH);
    }
    else{
      digitalWrite(RELAY2, LOW);
      closeDetect = false;
    }
    
    refresh_time = millis() + SERIAL_REFRESH_TIME;
    
    
  }
  
}
